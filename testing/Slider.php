<?php

use common\helpers\Languages;

$this->title = Languages::presentValue('Майнинг', 'Mining', 'Minería') . " - " . Yii::$app->params['title_name_all'];
?>
<main>
    <div class="lk">
        <div class="container">
            <div class="lk-info">
                <ul class="lk-btns">
                    <li><a href="/cabinet/mining"
                           class="btn btn--orange"><?= Languages::presentValue('Майнинг', 'Mining', 'Minería') ?></a>
                    </li>
                    <li><a href="/cabinet/payment"
                           class="btn btn--orange"><?= Languages::presentValue('Пополнить баланс', 'Top up balance', 'Saldo de recarga') ?></a>
                    </li>
                    <li><a href="/cabinet/history"
                           class="btn btn--orange"><?= Languages::presentValue('Платежи', 'Payments', 'Pagos') ?></a>
                    </li>
                    <li><a href="/cabinet/referrals/"
                           class="btn btn--orange"><?= Languages::presentValue('Партнерская программа', 'Affiliate program', 'Programa de afiliados') ?></a>
                    </li>
                </ul>
                <div class="lk-balance">
                    <span class="lk-balance__title"><?= Languages::presentValue('Баланс', 'Balance', 'Balance general') ?>
                        :</span>
                    <span class="lk-balance__lk-mh">
                        <?= \common\helpers\Alignment::numbersAfterDecimalPoint(Yii::$app->user->identity->power) ?>
                        <?= \common\models\InvestPlanSettings::getValue('name') ?>
                    </span>
                    <span class="lk-balance__plus">+</span>
                </div>
                <div class="lk-data">
                    <span class="lk-name"><?= \Yii::$app->user->identity->username ?></span>
                    <span class="lk-referal-link"><?= Languages::presentValue('Ваша реферальная ссылка', 'Your referral link', 'Su enlace de referencia') ?>
                        :<span><?= Yii::$app->params['protocol'] ?><?= $_SERVER['SERVER_NAME'] . "/?ref=" . \Yii::$app->user->identity->id ?></span></span>
                    <a href="/cabinet/payment"
                       class="btn btn--orange"><?= Languages::presentValue('Пополнить баланс', 'Top up balance', 'Saldo de recarga') ?></a>
                </div>
            </div>
            <h2><?= Languages::presentValue('Счета', 'Accounts', 'Cuentas') ?>:</h2>
            <div class="accounts">
                <?php foreach (\common\models\InvestPlanProduct::getAllArray() as $value): ?>
                    <?php $earningsPerHour = (new \common\models\User())->cursePerHours($value['curs']); ?>
                    <div class="account-item">
                        <div class="account-withdraw dollars-bg"
                             style="    background: #fff url(/images/<?= $value['images'] ?>) calc(100% + 30px) -30px no-repeat;">
                            <span class="account-withdraw__title"><?= $value['name'] ?> <span></span></span>
                            <span class="account-withdraw__balance" id="productAmount<?= $value['id'] ?>"></span>
                            <a href="/cabinet/withdraw/?curs=<?= $value['curs'] ?>"
                               class="btn btn--orange"><?= Languages::presentValue('Вывести', 'Withdraw', 'Retirarse') ?></a>
                            <div class="rabge-slider">
                                <input class="slider_<?= $value['id'] ?> "
                                       value="<?= \common\models\ProductDistributionShares::getPersonalSettings($value['curs']) ?>"
                                       min="0"
                                       data-curs="<?= $value['curs'] ?>" max="100"
                                       name="rangeslider-<?= \common\models\Accounts::getShortId($value['curs']) ?>"
                                       type="range" onchange="showVal(this)" oninput="updateSlider(this)"/>
                            </div>
                        </div>

                        <div class="earnings-forecast">
                            <div class="earnings-forecast__title"><?= Languages::presentValue('Прогноз заработка', 'Earnings forecast', 'Previsión de ganancias') ?></div>
                            <ul>
                                <li>1 <?= Languages::presentValue('час', 'hour', 'una hora') ?>
                                    <span><?= \common\helpers\Alignment::numbersAfterDecimalPoint($earningsPerHour); ?> <?= \common\models\Accounts::getShortId($value['curs']) ?></span>
                                </li>
                                <li>24 <?= Languages::presentValue('часа', 'hours', 'en punto') ?>
                                    <span><?= \common\helpers\Alignment::numbersAfterDecimalPoint($earningsPerHour * 24); ?> <?= \common\models\Accounts::getShortId($value['curs']) ?></span>
                                </li>
                                <li>1 <?= Languages::presentValue('месяц', 'month', 'mes') ?>
                                    <span><?= \common\helpers\Alignment::numbersAfterDecimalPoint($earningsPerHour * 744); ?> <?= \common\models\Accounts::getShortId($value['curs']) ?></span>
                                </li>
                                <li>3 <?= Languages::presentValue('месяца', 'months', 'del mes') ?>
                                    <span><?= \common\helpers\Alignment::numbersAfterDecimalPoint($earningsPerHour * 2232); ?> <?= \common\models\Accounts::getShortId($value['curs']) ?></span>
                                </li>
                                <li>1 <?= Languages::presentValue('год', 'year', 'año') ?>
                                    <span><?= \common\helpers\Alignment::numbersAfterDecimalPoint($earningsPerHour * 8928); ?> <?= \common\models\Accounts::getShortId($value['curs']) ?></span>
                                </li>
                            </ul>
                        </div>
                    </div>

                <?php $amountUser = \common\models\AccountsRelation::getAmountUserAll($value['curs']); ?>

                    <script language="javascript">
                        <?php if($amountUser === false):?>
                        document.getElementById("productAmount<?=$value['id']?>").innerHTML = "<?=\common\helpers\Languages::presentValue('Счет не активен', 'Invoice is not active', 'La cuenta no esta activa')?>";
                        <?php else:?>
                        (function () {
                            document.getElementById("productAmount<?=$value['id']?>").innerHTML = <?=$amountUser?>;
                            var writeTo = document.getElementById("productAmount<?=$value['id']?>");
                            var sec<?=$value['id']?> = <?=$amountUser?>;
                            var timeAmount = <?=\common\models\User::getCoursePerSecond($value['curs']) * (($value['interest_period_frequency'] != 0) ? ((int)$value['interest_period_frequency'] * 3600) : 1);?>;
                            setInterval(function () {
                                sec<?=$value['id']?> = sec<?=$value['id']?> + timeAmount;
                                writeTo.innerHTML = sec<?=$value['id']?>.toFixed(11);
                            }, <?=($value['interest_period_frequency'] != 0) ? ((int)$value['interest_period_frequency'] * 3600 * 1000) : 1000 ?>)
                        })();
                        <?php endif;?>

                    </script>
                <?php endforeach; ?>

            </div>
        </div>

    </div>


</main>
<script>
    var lk = '<?=Yii::$app->params["account"]?>';
</script>
<?php
$js = <<<JS

$("document").ready(function () {
   $(".slider_1").rangeslider();
   $(".slider_2").rangeslider();
   $(".slider_3").rangeslider();
});
$.fn.rangeslider = function (options) {
    var obj = this;
    var defautValue = obj.attr("value");
    obj.wrap("<span class='range-slider'></span>");
    obj.after("<span class='slider-container'><span class='bar'><span></span></span><span class='bar-btn'><span>0</span></span></span>");
    obj.attr("oninput", "updateSlider(this)");
    updateSlider(this);
    return obj;
};

function updateSlider(passObj,val = null) {
    var obj = $(passObj);
    if(val == null){
        var value = obj.val();
    }else{
         var value = val;
    }
    
     //console.log(value);
    var min = obj.attr("min");
    var max = obj.attr("max");
    var range = Math.round(max - min);
    var percentage = Math.round((value - min) * 100 / range);
    var nextObj = obj.next();
    nextObj.find("span.bar-btn").css("left", percentage + "%");
    nextObj.find("span.bar > span").css("width", percentage + "%");
    nextObj.find("span.bar-btn > span").text(percentage);
};
 
function showVal(obj) {
          var ltc  =  Number($('[name="rangeslider-LTC"]').val());
          var doge =  Number($('[name="rangeslider-DOGE"]').val());
          var usd  =  Number($('[name="rangeslider-USD"]').val());
         
          
         
         if(ltc < 0 || doge < 0 || usd < 0){
             return false;
         }
         
         //Остаток после превышения
         var amountRemainder = Number(amount())-100;
         //var amount = 0;
 
         if(amountRemainder > 0 && ltc > 0 && obj.name != 'rangeslider-LTC'){
                 var amount_ltc = ltc - amountRemainder;
                 if(amount_ltc > 0){
                     $('[name="rangeslider-LTC"]').attr("value", amount_ltc);
                     this.updateSlider($('[name="rangeslider-LTC"]'), amount_ltc);
                     amountRemainder = 0;
                 }else{
                     $('[name="rangeslider-LTC"]').attr("value", 0);
                    amountRemainder = amountRemainder - ltc;
                    this.updateSlider($('[name="rangeslider-LTC"]'), 0);
                 }
         }
          if(amountRemainder > 0  && doge > 0 && obj.name != 'rangeslider-DOGE'){
                 var amount_doge = doge - amountRemainder;
                 if(amount_doge > 0){
                     $('[name="rangeslider-DOGE"]').attr("value", amount_doge);
                     this.updateSlider($('[name="rangeslider-DOGE"]'), amount_doge);
                     amountRemainder = 0;
                 }else{
                     $('[name="rangeslider-DOGE"]').attr("value", 0);
                     amountRemainder = amountRemainder - doge;
                     this.updateSlider($('[name="rangeslider-DOGE"]'), 0);
                 }
         }
          if(amountRemainder > 0  && usd > 0 && obj.name != 'rangeslider-USD'){
                 var amount_usd = usd - amountRemainder;
                 if(amount_usd > 0){
                     $('[name="rangeslider-USD"]').attr("value", amount_usd);
                     this.updateSlider($('[name="rangeslider-USD"]'), amount_usd);
                     amountRemainder = 0;
                 }else{
                    $('[name="rangeslider-USD"]').attr("value", 0);
                    amountRemainder = amountRemainder - usd;
                    this.updateSlider($('[name="rangeslider-USD"]'), 0);
                 }
         }
         
         if(amountRemainder > 0){
             return false;
         }
         

   $.ajax({
            url: lk+'/mining/custom-product-settings',
            data: 'type=2'+
            "&LTC_10="+Number($('[name="rangeslider-LTC"]').val())+
            "&DOGE_11="+Number($('[name="rangeslider-DOGE"]').val())+
            "&USD_12="+Number($('[name="rangeslider-USD"]').val()),
            type: "POST",
            dataType: "json",
            success: function (msg) {
                if(msg == 1){
                   location.reload(true); 
                }
            }
        });
}

// подсчет общей суммы 
  function amount() 
  {
      //$(document).ready( function() {
          var ltc = Number($('[name="rangeslider-LTC"]').val());
          var doge = Number($('[name="rangeslider-DOGE"]').val());
          var usd = Number($('[name="rangeslider-USD"]').val());
        
     // });
      
      return ltc + doge + usd;
  }
JS;
$this->registerJs($js, \yii\web\View::POS_END);
?>
