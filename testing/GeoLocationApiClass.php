<?php

/**
 *
 *
 *  Example
 *  $modelLocation = new GeoLocation ('your key');
 *  $data = $modelLocation->request('city', 'www.google.com');
 *  var_dump($data);
 */

class GeoLocation
{
    protected $apiKey;

    public $urlApi = 'https://ip-location.icu/api/v1/';


    function __construct($apiKey)
    {
        if (empty($apiKey)) {
            throw new InvalidArgumentException('apiKey is empty');
        }

        $this->apiKey = $apiKey;
    }


    /**
     * @param string $method  - method of connection
     * @param string $ip - IP address in format IpV4, IpV6, domain names
     * @param string $format - output format available JSON, XML
     * @param int $timeout - Request execution time, 5 seconds by default
     * @return array
     */
    public function request(string $method, $ip = '', $format = 'json', $timeout = 5)
    {

        $url = $this->urlApi . $method . '?apiKey=' . $this->apiKey . '&ip=' . $ip . '&format=' . $format;

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, true);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($curl, CURLOPT_TIMEOUT, $timeout);
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, $timeout);
        $response = curl_exec($curl);
        curl_close($curl);


        return json_decode($response, true);
    }

}