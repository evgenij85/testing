<?php

namespace app\modules\payments\controllers;

use common\models\Payments;
use common\models\Tarifs;
use common\models\User;
use app\components\LiqPay;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\HttpException;

/**
 * Default controller for the `payments` module
 */
class DefaultController extends Controller
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['send', 'index', 'success'],
                'rules' => [
                    [
                        'actions' => ['send', 'index', 'success'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['success'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],


                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'success' => ['post'],
                ],

            ],
        ];
    }

    public function beforeAction($action)
    {
        if ($action->id == 'success') {
            $this->enableCsrfValidation = false;
        }

        return parent::beforeAction($action);
    }

    /**
     * Renders the index view for the module
     * @return string
     */

    public function actionSend()
    {

        if ($post = \Yii::$app->request->post()) {

            if (empty($post['price'])) {
                return $this->redirect('/');
            }

            $model = \app\models\Tarifs::findOne(['price' => $post['price']]);
            if (!$model) {
                throw new HttpException(404, 'Page not found');
            }

            $liqpay = new LiqPay(\Yii::$app->params['liq_pay']['public_key'], \Yii::$app->params['liq_pay']['private_key']);
            $html = $liqpay->cnb_form(array(
                'action' => 'pay',
                'amount' => $model->price,
                'currency' => 'USD',
                //   'sandbox'        => 1, //тестирование платежа
                'description' => 'Increase of amount of connecting of API to '.$model->count,
                'order_id' => \app\models\Payments::getPay($model->price),
                'version' => '3',
                'language' => 'en',
                'server_url' => 'https://' . $_SERVER['SERVER_NAME'] . '/payments/success'
            ));

            return $this->render('index', [
                'html' => $html
            ]);

        }

        throw new HttpException(404, 'Page not found');

    }

    public function actionSuccess()
    {
        //$this->enableCsrfValidation = false;

        if ($post = \Yii::$app->request->post()) {

            $signature = base64_encode(sha1(\Yii::$app->params['liq_pay']['private_key'] . $post['data'] .
                \Yii::$app->params['liq_pay']['private_key']
                , 1));

            $post_data = base64_decode($post['data']);
            $data = json_decode($post_data, true);


            if ($signature == $post['signature']) {
                //оплата успешно
                $model = \app\models\Payments::findOne((int)$data['order_id']);

                if (!$model) {
                    \Yii::warning('LiqPay error payments not found: ' . serialize($data));
                    return false;
                }
                if ($model->amount == $data['amount'] && $model->status == \app\models\Payments::PAYMENTS_STATUS_CREATED) {
                    $model->status = \app\models\Payments::PAYMENTS_STATUS_SUCCESSFUL;
                } else {
                    $model->status = \app\models\Payments::PAYMENTS_STATUS_UNEXPLAINED;
                    \Yii::warning('LiqPay error' . serialize($data));
                }
                if ($model->save()) {
                    $profile = \app\models\Profile::findOne(['user_id' => $model->user_id]);
                    $api_limit = \app\models\Tarifs::getPriceCount((int)$data['amount']);

                    if ($api_limit !== null && $profile->api_limit < $api_limit) {
                        $profile->api_limit = $api_limit;
                        $profile->save();
                    }

                }
                return true;

            }
        }
        throw new HttpException(404, 'Page not found');
    }
}

