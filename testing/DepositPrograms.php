<?php

namespace common\models;

use common\models\ProductDiscounts;
use Yii;

/**
 * This is the model class for table "{{%deposit_programm}}".
 *
 * @property int $deposit_id
 * @property int $deposit_user_id Партнер
 * @property int $deposit_product_discounts_id Тариф
 * @property int $deposit_date_start
 * @property int $deposit_date_end Срок окончания депозита в формате таймтсап
 * @property int $deposit_payments_id id пополнения
 * @property int $deposit_status Статус 1 Активен 2 Завершен 3 Отмена
 * @property double $deposit_account Сумма начисления в usd
 * @property int $deposit_withdrawal_status Статус вывода 1 начислено 2 выведено
 * @property string $deposit_create_at Дата создания
 */
class DepositPrograms extends \yii\db\ActiveRecord
{
    const STATUS_ACTIVE = 1; //Активный начисляется
    const STATUS_COMPLETED = 2; //Завершен не начисляется
    const STATUS_CANCELED = 3; //Отменен не начисляется

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%deposit_programs}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['deposit_user_id', 'deposit_product_discounts_id', 'deposit_date_start', 'deposit_date_end', 'deposit_payments_id','deposit_invest_plan_product_id'], 'required'],
            [['deposit_user_id', 'deposit_product_discounts_id', 'deposit_date_start', 'deposit_date_end', 'deposit_payments_id', 'deposit_status', 'deposit_withdrawal_status','deposit_invest_plan_product_id'], 'integer'],
            [['deposit_account'], 'number'],
            [['deposit_create_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'deposit_id' => 'Deposit ID',
            'deposit_user_id' => 'Deposit User ID',
            'deposit_product_discounts_id' => 'Deposit Product Discounts ID',
            'deposit_date_start' => 'Deposit Date Start',
            'deposit_date_end' => 'Deposit Date End',
            'deposit_payments_id' => 'Deposit Payments ID',
            'deposit_invest_plan_product_id' => 'Product ID',
            'deposit_status' => 'Deposit Status',
            'deposit_account' => 'Deposit Account',
            'deposit_withdrawal_status' => 'Deposit Withdrawal Status',
            'deposit_create_at' => 'Deposit Create At',
            'deposit_amount_per_days' => 'Сумма начисления в день согласно персональных настроек'
        ];
    }

    public function getProductDiscounts()
    {
        return $this->hasOne(ProductDiscounts::className(), ['id' => 'deposit_product_discounts_id']);
    }

    public function getPayments()
    {
        return $this->hasOne(Payments::className(), ['id' => 'deposit_payments_id']);
    }

    public function getInvestPlanProduct()
    {
        return $this->hasOne(InvestPlanProduct::className(), ['id' => 'deposit_invest_plan_product_id']);
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'deposit_user_id']);
    }

    public static function getStatus($status = null)
    {
        $value =[
            self::STATUS_ACTIVE => 'Активен',
            self::STATUS_COMPLETED => 'Завершен',
            self::STATUS_CANCELED => 'Отменен'
        ];

        if($status !== null){
            if(isset($value[$status])){
                return $value[$status];
            }
            return 'null';
        }

        return $value;

    }

    /**
     * @param $modelPayments
     * Создание депозита при пополнении
     */
    public static function create($modelPayments)
    {
        if(!$discounts = ProductDiscounts::getDataBetween($modelPayments->amount_usd)){
            Logging::getRecording(
                Logging::DEPOSIT_PROGRAM,
                'Не удалось определить дисконт начисления при создании депозита {{%deposit_programm}}, id платежа: '.$modelPayments->id.', сумма: '.$modelPayments->amount_usd);

        }

        $model = new self();
        $model->deposit_user_id = $modelPayments->user_id;
        $model->deposit_product_discounts_id = $discounts['id'];
        $model->deposit_invest_plan_product_id = $discounts['product_id'];
        $model->deposit_payments_id = $modelPayments->id;
        $model->deposit_date_start = time();
        $model->deposit_account = self::getBonus($discounts['product_id'], $modelPayments->amount_usd);
        $model->deposit_date_end = $model->deposit_date_start + (int)$discounts['count_days'] * 86400;
         if(!$model->save()){
             Logging::getRecording(
                 Logging::DEPOSIT_PROGRAM,
                 'Не удалось сохранить депозит в таблице {{%deposit_programm}} modelPayments:'.serialize((array)$modelPayments).'. Error:'.serialize((array)$model->getErrors()));
         }
    }

    /**
     * @param $product_id
     * @param $amount_usd
     * @return float|int
     * Бонус при пополнениии
     */
    public static function getBonus($product_id, $amount_usd)
    {
        $model = InvestPlanProduct::findOne($product_id);
        if($model && isset($model->bonus_type, $model->multiple_bonus_payments_size) && $model->multiple_bonus_payments_size > 0){
            return $amount_usd*$model->multiple_bonus_payments_size/100;
        }

        return 0;

    }


    public static function restart()
    {
        $modelDeposit = DepositPrograms::find()
            ->select('*')
            ->with('productDiscounts', 'payments','investPlanProduct')
            ->where(['deposit_user_id' => Yii::$app->user->id])
            ->andWhere(['deposit_status' => DepositPrograms::STATUS_ACTIVE])
            //->andWhere([InvestPlanProduct::tableName().'.status' => InvestPlanProduct::STATUS_ACTIVE])
            ->all();

        foreach ($modelDeposit as $model) {

            $currentTime = time();

            //проверем статус платежа пополнения, если статус платежа отличается от успешный то не начисляем проценты
             if(!isset($model->payments, $model->investPlanProduct) || $model->payments->status <> Payments::STATUS_SUCCESS || $model->investPlanProduct->status <> InvestPlanProduct::STATUS_ACTIVE){
                continue;
             }

            //Считаем сколько в секунду начисляем
            $chargeAmountPerSecond = round(($model->payments->amount_usd * $model->productDiscounts->percent / 100) / 86400, 15, PHP_ROUND_HALF_DOWN);

            //проверяем не закончился срок депозита
            if ($currentTime >= $model->deposit_date_end) {   //Завершаем процес дозачислем остаток и ставим статус завершен

                //определяемкакой остоок остался не зачисленным
                $countTime = $model->deposit_date_end - $model->deposit_date_start;
                if ($countTime < 0) {
                    continue;
                }

                //считаем сколько нужно выплатить остатк, фиксируем начисление и закрываем депозит
                $countAmount = $chargeAmountPerSecond * $countTime;
                if ($countAmount < 0) {
                    continue;
                }

                $model->deposit_account += $countAmount;
                $model->deposit_status = self::STATUS_COMPLETED;
                $model->deposit_date_start = $model->deposit_date_end;
                $model->save();

            } else { //депозит активный начинаем процес начисления
                //определяем сколько прошло с момента последненго обновления (секунд)
                $past_elapsed_time_from_current = $currentTime - $model->deposit_date_start;
                if ($past_elapsed_time_from_current < 0) {
                    continue;
                }

                //узнаем пришло ли расчетнове время если с момента последненго обновления прошло меньше времени чем установлено то завершаем процесс
                $timeNewUpdate = $model->investPlanProduct->interest_period_frequency;
                if ($timeNewUpdate > 0 && $past_elapsed_time_from_current < $timeNewUpdate * 3600) {
                    continue;
                }

                //считаем сколько нужно выплатить и фиксируем начисления
                $countAmount = $chargeAmountPerSecond * $past_elapsed_time_from_current;
                if ($countAmount < 0) {
                    continue;
                }

                $model->deposit_account += $countAmount;
                $model->deposit_date_start = $currentTime;
                $model->save();
            } //endif

        } //end foreach

    }




}
